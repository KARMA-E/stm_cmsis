#include "usart.h"

#include "stdio.h"
#include <stdarg.h>
#include <string.h>


#define _RX_BUF_SIZE	(64)


static uint8_t  _rx_buf[_RX_BUF_SIZE];
static uint16_t _rx_buf_in_ind = 0;
static uint16_t _rx_buf_out_ind = 0;


void USART_init(USART_TypeDef * USARTx, const uint32_t baudrate)
{
	if(USARTx == USART1)
	{
		RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
		RCC->APB2ENR |= RCC_APB2ENR_IOPBEN;
		RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;

		AFIO->MAPR |= AFIO_MAPR_USART1_REMAP;

		MODIFY_REG(GPIOB->CRL, GPIO_CRL_MODE6_Msk, GPIO_MODE_10_MHZ << GPIO_CRL_MODE6_Pos);
		MODIFY_REG(GPIOB->CRL, GPIO_CRL_CNF6_Msk,  GPIO_CNF_ALT_PP 	<< GPIO_CRL_CNF6_Pos);

		MODIFY_REG(GPIOB->CRL, GPIO_CRL_MODE7_Msk, GPIO_MODE_INPUT << GPIO_CRL_MODE7_Pos);
		MODIFY_REG(GPIOB->CRL, GPIO_CRL_CNF7_Msk,  GPIO_CNF_INPUT  << GPIO_CRL_CNF7_Pos);
	}

	uint32_t usart_brr = (FREQ_APB2_MHZ * 1000000) / baudrate;
	//uint32_t mantissa = usart_brr / 16;
	//uint32_t fraction = usart_brr % 16;
	USARTx->BRR = usart_brr;

	USARTx->CR1 = 0;
	USARTx->CR1 |= USART_CR1_TE | USART_CR1_RE;
	USARTx->CR1 |= USART_CR1_UE;

	USARTx->CR1 |= USART_CR1_RXNEIE;
	NVIC_EnableIRQ(USART1_IRQn);

	USARTx->SR;

	DBG("\r\n\r\n--------------------- START ---------------------");
	DBG_FILE_INFO();
}

void USART_Send(USART_TypeDef * USARTx, const uint8_t data)
{
	USARTx->DR = (uint32_t)data;
	while((USARTx->SR & USART_SR_TC) != USART_SR_TC){}
}

void USART_Send_buf(USART_TypeDef * USARTx, const uint8_t * buf, const uint16_t len)
{
	for(uint16_t ind = 0; ind < len; ind++)
	{
		USART_Send(USARTx, buf[ind]);
	}
}

void USART_Debug_printf(const char * str, ...)
{
	va_list args;
	va_start(args, str);

	uint8_t send_buf[200];
	vsprintf((char*)send_buf, str, args);
	USART_Send_buf(USART1, send_buf, strlen((char*)send_buf));

	va_end(args);
}

uint8_t USART_Get_rx_data(USART_TypeDef * USARTx)
{
	uint8_t res = 0x00;

	if(_rx_buf_in_ind != _rx_buf_out_ind)
	{
		res = _rx_buf[_rx_buf_out_ind];
		(_rx_buf_out_ind == (_RX_BUF_SIZE - 1)) ? (_rx_buf_out_ind = 0) : _rx_buf_out_ind++;
	}
	return res;
}

uint16_t USART_Get_rx_qty(USART_TypeDef * USARTx)
{
	return (_rx_buf_in_ind + ((_rx_buf_out_ind <= _rx_buf_in_ind) ? 0 : _RX_BUF_SIZE)) - _rx_buf_out_ind;
}

void USART1_IRQHandler(void)
{
	if((USART1->SR & USART_SR_RXNE) == USART_SR_RXNE)
	{
		_rx_buf[_rx_buf_in_ind] = USART1->DR;
		(_rx_buf_in_ind == (_RX_BUF_SIZE - 1)) ? (_rx_buf_in_ind = 0) : _rx_buf_in_ind++;
	}
}
