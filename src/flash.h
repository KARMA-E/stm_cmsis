#ifndef _FLASH_H_
#define _FLASH_H_

#include "ginc.h"


#define FLASH_USER_DATA_ADDR	(0x08010000)
#define FLASH_USER_DATA_SIZE	(0x10000)
#define FLASH_PAGE_SIZE			(0x400)
#define FLASH_USER_PAGES_QTY	(FLASH_USER_DATA_SIZE / FLASH_PAGE_SIZE)
#define FLASH_INVALID_DWORD		(0xFFFFFFFF)


void 	 FLASH_Unlock(void);
void 	 FLASH_Lock(void);
uint8_t  FLASH_Check_ready(void);

void 	 FLASH_Erase_page(uint32_t addr);
uint32_t FLASH_Read(uint32_t addr);
void 	 FLASH_Write(uint32_t addr, uint32_t data);

void 	 FLASH_Read_buf(uint32_t addr, uint32_t * buf, uint16_t len);
void 	 FLASH_Write_buf(uint32_t addr, const uint32_t * buf, uint16_t len);

#endif
