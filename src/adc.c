#include "adc.h"
#include "usart.h"


#define _ADC_CH_QTY			(18)
#define _INJ_SQ_MAX_QTY		(4)
#define _REG_SQ_MAX_QTY		(16)
#define _TSENS_INIT_VAL		(1909)	// 0
#define _VREFINT_INIT_VAL	(1490)
#define	_TEMP_CALC_ACCUR	(2)


static uint8_t _regular_seq_qty = 1;
static uint8_t _injected_seq_qty = 1;


uint8_t ADC_Init(ADC_TypeDef * ADCx)
{
	uint8_t res = 0;

	if(ADCx == ADC1)
	{
		RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
	}
	else if(ADCx == ADC2)
	{
		RCC->APB2ENR |= RCC_APB2ENR_ADC2EN;
	}
	else
	{
		res = 1;
	}

	if(res != 1)
	{
		ADCx->CR2 = 0;
		ADCx->SMPR1 = 0;
		ADCx->SMPR2 = 0;
		ADCx->SQR1 = 0;
		ADCx->SQR2 = 0;
		ADCx->SQR3 = 0;

		ADCx->CR2 |= ADC_CR2_CAL;
		ADCx->CR2 |= ADC_CR2_RSTCAL;
		while((ADCx->CR2 & ADC_CR2_CAL) == ADC_CR2_CAL){}

		ADCx->CR2 |= ADC_CR2_TSVREFE;

		//ADCx->CR2 |= ADC_CR2_CONT;
		//ADCx->CR1 |= ADC_CR1_DISCEN;
		//MODIFY_REG(ADCx->CR1, ADC_CR1_DISCNUM_Msk, 1 - 1);

		ADCx->CR2 |= ADC_CR2_EXTTRIG;
		MODIFY_REG(ADCx->CR2, ADC_CR2_EXTSEL_Msk, ADC_CR2_EXTSEL_Msk);

		ADCx->CR2 |= ADC_CR2_JEXTTRIG;
		MODIFY_REG(ADCx->CR2, ADC_CR2_JEXTSEL_Msk, ADC_CR2_JEXTSEL_Msk);

		for(uint8_t ch = 0; ch < _ADC_CH_QTY; ch++)
		{
			// 41.5 cycles
			ADC_Set_channel_sample(ADCx, ch, ADC_SMPR2_SMP0_2);
		}

		ADCx->CR2 |= ADC_CR2_ADON;

		// ADC setup example to measure temperature
		ADC_Set_channel_rsq(ADC1, 17, 0);
		ADC_Set_rsq_qty(ADC1, 1);

		ADC_Set_channel_jsq(ADC1, 16, 0);
		ADC_Set_channel_jsq(ADC1, 17, 1);

		ADC_Set_jsq_qty(ADC1, 2);
	}

	DBG_FILE_INFO();
	return res;
}


uint8_t ADC_Set_channel_sample(ADC_TypeDef * ADCx, uint8_t channel, uint8_t smp)
{
	uint8_t res = 0;

	if(channel < 10)
	{
		MODIFY_REG(ADCx->SMPR2, ADC_SMPR2_SMP0_Msk << (channel * 3), ((uint32_t)smp) << (channel * 3));
	}
	else
	{
		channel -= 10;
		MODIFY_REG(ADCx->SMPR1, ADC_SMPR1_SMP10_Msk << (channel * 3), ((uint32_t)smp) << (channel * 3));
	}

	return res;
}


uint8_t ADC_Set_channel_rsq(ADC_TypeDef * ADCx, uint8_t channel, uint8_t sq)
{
	uint8_t res = 0;
	sq = (ADCx == ADC2) ? ((_REG_SQ_MAX_QTY - 1) - sq) : sq;

	if(sq < 7)
	{
		MODIFY_REG(ADCx->SQR3, ADC_SQR3_SQ1_Msk << (sq * 5), ((uint32_t)channel) << (sq * 5));
	}
	else if(sq < 13)
	{
		sq -= 7;
		MODIFY_REG(ADCx->SQR2, ADC_SQR2_SQ7_Msk << (sq * 5), ((uint32_t)channel) << (sq * 5));
	}
	else
	{
		sq -= 13;
		MODIFY_REG(ADCx->SQR1, ADC_SQR1_SQ13_Msk << (sq * 5), ((uint32_t)channel) << (sq * 5));
	}

	return res;
}

uint8_t ADC_Set_channel_jsq(ADC_TypeDef * ADCx, uint8_t channel, uint8_t sq)
{
	uint8_t res = 0;
	sq = (ADCx == ADC1) ? ((_INJ_SQ_MAX_QTY - 1) - sq) : sq;

	MODIFY_REG(ADCx->JSQR, ADC_JSQR_JSQ1_Msk << (sq * 5), ((uint32_t)channel) << (sq * 5));

	return res;
}


uint8_t ADC_Set_rsq_qty(ADC_TypeDef * ADCx, uint8_t qty)
{
	uint8_t res = 0;

	ADCx->CR1 = (qty > 1) ? (ADCx->CR1 | ADC_CR1_SCAN) : (ADCx->CR1 & (~ADC_CR1_SCAN));
	ADCx->CR2 = (qty > 1) ? (ADCx->CR2 | ADC_CR2_DMA)  : (ADCx->CR2 & (~ADC_CR2_DMA));
	MODIFY_REG(ADCx->SQR1, ADC_SQR1_L_Msk, ((uint32_t)qty - 1) << ADC_SQR1_L_Pos);

	_regular_seq_qty = qty;

	return res;
}

uint8_t ADC_Set_jsq_qty(ADC_TypeDef * ADCx, uint8_t qty)
{
	uint8_t res = 0;

	ADCx->CR1 = (qty > 1) ? (ADCx->CR1 | ADC_CR1_SCAN) : (ADCx->CR1 & (~ADC_CR1_SCAN));
	ADCx->CR2 = (qty > 1) ? (ADCx->CR2 | ADC_CR2_DMA)  : (ADCx->CR2 & (~ADC_CR2_DMA));
	MODIFY_REG(ADCx->JSQR, ADC_JSQR_JL, ((uint32_t)qty - 1) << ADC_JSQR_JL_Pos);

	_injected_seq_qty = qty;

	return res;
}


void ADC_Start_regular(ADC_TypeDef * ADCx)  {ADCx->CR2 |= ADC_CR2_SWSTART;}
void ADC_Start_injected(ADC_TypeDef * ADCx) {ADCx->CR2 |= ADC_CR2_JSWSTART;}


uint8_t ADC_Get_regular(ADC_TypeDef * ADCx, uint16_t * val)
{
	uint8_t res = 0;

	if((ADCx->SR & ADC_SR_EOC) == ADC_SR_EOC)
	{
		*val = ADCx->DR;
	}
	else
	{
		res = 1;
	}

	return res;
}

uint8_t ADC_Get_injected(ADC_TypeDef * ADCx, uint16_t * val)
{
	uint8_t res = 0;

	if((ADCx->SR & ADC_SR_JEOC) == ADC_SR_JEOC)
	{
		for(uint8_t conv = 0; conv < _injected_seq_qty; conv++)
		{
			if(ADCx == ADC1)
			{
				*(val + conv) = *( (uint32_t*) &(ADCx->JDR1) + (_injected_seq_qty - 1) - conv);
			}
			else if(ADCx == ADC2)
			{
				*(val + conv) = *( (uint32_t*) &(ADCx->JDR1) + conv);
			}
			else
			{
				res = 1;
			}
		}
	}
	else
	{
		res = 1;
	}

	return res;
}


uint8_t ADC_Get_temp(ADC_TypeDef * ADCx, int16_t * temp_int, uint8_t * temp_fract)
{
	static int32_t tsens_val = _TSENS_INIT_VAL   << _TEMP_CALC_ACCUR;
	static int32_t vref_val  = _VREFINT_INIT_VAL << _TEMP_CALC_ACCUR;

	uint16_t adc_val[4] = {0};

	for(uint8_t itr = 0; itr < 128; itr++)
	{
		ADC_Start_injected(ADC1);
		while(ADC_Get_injected(ADC1, adc_val)){}

		tsens_val = (int32_t)((tsens_val * 31) + (adc_val[0] << _TEMP_CALC_ACCUR)) / 32;
		vref_val  = (int32_t)((vref_val  * 31) + (adc_val[1] << _TEMP_CALC_ACCUR)) / 32;
	}

	//Vrefint 	= 1.2 V
	//V25 		= 1.43 V
	//Avr 		= 4.3 mV/C
	//T = ((V25 - VSENSE) / Avg_Slope) + 25.

	int32_t v_25      = (vref_val * 14300)  / 12000;
	int32_t avr_slope = (vref_val * 43) 	/ 12000;

	int32_t temp = ( ((v_25 - tsens_val) * 10) / avr_slope) + (25 * 10);

	*temp_int 	= temp / 10;
	*temp_fract	= (uint8_t)(((temp < 0) ? -temp : temp) % 10);

	return 0;
}

