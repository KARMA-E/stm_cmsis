#include "ginc.h"

#include "system.h"
#include "usart.h"
#include "flash.h"
#include "adc.h"
#include "dma.h"
#include "sdio.h"

#include "flash_test.h"


#define TOGGLE_LED	GPIOC->ODR ^= GPIO_ODR_ODR13


int main(void)
{
	SYS_init();
	USART_init(USART1, 921600);
	ADC_Init(ADC1);
	FLASH_Unlock();
	DMA_Init();

	DBG_FILE_INFO();
	FL_TEST_Read_statistic();

	while(1)
	{
		static volatile uint32_t old_time = 0;
		uint32_t new_time = SYS_Get_tick_ms();
		if((new_time - old_time) > 50)
		{
			old_time = new_time;
			TOGGLE_LED;
		}

		FL_TEST_Pull();

		uint8_t usart_rx_data = USART_Get_rx_data(USART1);

		if(usart_rx_data == '1')
		{
			TIME_TEST(FL_TEST_Check_page(0, 340));
		}
		else if(usart_rx_data == '2')
		{
			DBG("\r\nCheck ON");
			FL_TEST_Set_enable(1);
		}
		else if(usart_rx_data == '3')
		{
			FL_TEST_Save_statistic();
			DBG("\r\nCheck OFF");
			FL_TEST_Set_enable(0);
		}
		else if(usart_rx_data == 'w')
		{
			FL_TEST_Save_statistic();
		}
		else if(usart_rx_data == 'r')
		{
			FL_TEST_Read_statistic();
		}
		else if(usart_rx_data == 'e')
		{
			DBG("\r\nDelete all statistic? (y/n)");
			while((usart_rx_data = USART_Get_rx_data(USART1)) == 0) {}

			if(usart_rx_data == 'y')
			{
				FL_TEST_Delete_statistic();
			}
			else
			{
				DBG(" break");
			}
		}
		else if(usart_rx_data == 'p')
		{
			TIME_TEST(FL_TEST_Print_statistic());
		}
		else if(usart_rx_data == 'a')
		{
			int16_t t_int;
			uint8_t t_fract;
			TIME_TEST(ADC_Get_temp(ADC1, &t_int, &t_fract));
			DBG("\r\nTEMP %d.%d", t_int, t_fract);
		}
		else if(usart_rx_data == 'd')
		{
			DBG("\r\nSend data to GPIOA by DMA");
#define DMA_DATA_SIZE 	(12)
			uint32_t dma_data[DMA_DATA_SIZE] = {1, 6, 3, 14, 5, 98, 7, 12, 33, 66, 23, 10};
			DMA_M2P_Start((uint32_t)dma_data, (uint32_t)(&GPIOA->ODR), DMA_DATA_SIZE);
			uint_fast16_t wait = 0xFFF;
			while(--wait);
		}
		else if(usart_rx_data == 'c')
		{
			DBG("\r\nSend data to GPIOA by CPU");
			volatile uint32_t dma_data[12] = {1, 6, 3, 14, 5, 98, 7, 12, 33, 66, 23, 10};
			GPIOA->ODR = dma_data[0];
			GPIOA->ODR = dma_data[1];
			GPIOA->ODR = dma_data[2];
			GPIOA->ODR = dma_data[3];
			GPIOA->ODR = dma_data[4];
			GPIOA->ODR = dma_data[5];
			GPIOA->ODR = dma_data[6];
			GPIOA->ODR = dma_data[7];
			GPIOA->ODR = dma_data[8];
			GPIOA->ODR = dma_data[9];
			GPIOA->ODR = dma_data[10];
			GPIOA->ODR = dma_data[11];
		}
		else if(usart_rx_data == 'z')
		{
			asm("b UsageFault_Handler;");
		}
	}
}





