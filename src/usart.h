#ifndef _USART_H_
#define _USART_H_

#include "ginc.h"

#define  DBG			USART_Debug_printf
#define  DBG_FILE_INFO() 	DBG("\r\n%s %s %s (%s)", __DATE__, __TIME__, __FILE__, __FUNCTION__)

void 	 USART_init(USART_TypeDef * USARTx, const uint32_t baudrate);
void 	 USART_Send(USART_TypeDef * USARTx, const uint8_t data);

void 	 USART_Send_buf(USART_TypeDef * USARTx, const uint8_t * buf, const uint16_t len);
void 	 USART_Debug_printf(const char * str, ...);
uint8_t  USART_Get_rx_data(USART_TypeDef * USARTx);
uint16_t USART_Get_rx_qty(USART_TypeDef * USARTx);

#endif
