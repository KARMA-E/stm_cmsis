#ifndef _GINC_H_
#define _GINC_H_

#include <stdint.h>
#include "stm32f1xx.h"

#define _SI static inline
//#define true 	(1)
//#define false 	(0)
//typedef uint8_t uint8_t;

#define FREQ_HSE_MHZ		(8U)
#define FREQ_SYS_MHZ		(48U)
#define PLL_MULT_VAL		(FREQ_SYS_MHZ / FREQ_HSE_MHZ)

#define PPRE1_DIV 			(RCC_CFGR_PPRE1_DIV1)
#define FREQ_APB1_MHZ		(FREQ_SYS_MHZ)

#define PPRE2_DIV 			(RCC_CFGR_PPRE2_DIV1)
#define FREQ_APB2_MHZ		(FREQ_SYS_MHZ)

#define ADCPRE_DIV 			(RCC_CFGR_ADCPRE_DIV4)
#define FREQ_ADC_MHZ		(FREQ_APB2_MHZ / 4)

#define GPIO_CR_BITS_PER_PIN	(4)

#define GPIO_MODE_Msk			(0x3)
#define GPIO_MODE_INPUT			(0x0)
#define GPIO_MODE_10_MHZ		(0x1)
#define GPIO_MODE_2_MHZ			(0x2)
#define GPIO_MODE_50_MHZ		(0x3)

// Input CNF
#define GPIO_CNF_ANALOG			(0x0)
#define GPIO_CNF_INPUT			(0x1)

// Output CNF
#define GPIO_CNF_Msk			(0x3)
#define GPIO_CNF_GEN_PP			(0x0)
#define GPIO_CNF_GEN_OD			(0x1)
#define GPIO_CNF_ALT_PP			(0x2)
#define GPIO_CNF_ALT_OD			(0x3)

#endif
