/*
 * sdio.h
 *
 *  Created on: 24 мая 2022 г.
 *      Author: KARMA
 */

#ifndef SRC_SDIO_H_
#define SRC_SDIO_H_

void SDIO_Init(void);
void SDIO_TestCs(void);

#endif /* SRC_SDIO_H_ */
