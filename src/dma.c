/*
 * dma.c
 *
 *  Created on: 30 мая 2022 г.
 *      Author: KARMA
 */

#include "dma.h"
#include "usart.h"


void DMA_Init(void)
{
	RCC->AHBENR |= RCC_AHBENR_DMA1EN;

	MODIFY_REG(DMA1_Channel1->CCR, DMA_CCR_PL_Msk, DMA_CCR_PL_Msk);
	MODIFY_REG(DMA1_Channel1->CCR, DMA_CCR_MSIZE_Msk, DMA_CCR_MSIZE_1);
	MODIFY_REG(DMA1_Channel1->CCR, DMA_CCR_PSIZE_Msk, DMA_CCR_PSIZE_1);
	DMA1_Channel1->CCR |= DMA_CCR_MEM2MEM | DMA_CCR_MINC | DMA_CCR_DIR;

	DMA1_Channel1->CCR |= DMA_CCR_TCIE;
	NVIC_EnableIRQ(DMA1_Channel1_IRQn);

	DBG_FILE_INFO();
}

void DMA_M2P_Start(uint32_t mem_addr, uint32_t perif_addr, uint16_t data_qty)
{
	DMA1_Channel1->CNDTR = data_qty;
	DMA1_Channel1->CMAR	 = mem_addr;
	DMA1_Channel1->CPAR  = perif_addr;

	DMA1_Channel1->CCR |= DMA_CCR_EN;
}

void DMA1_Channel1_IRQHandler(void)
{
	if((DMA1->ISR & DMA_ISR_TCIF1) == DMA_ISR_TCIF1)
	{
		DMA1->IFCR |= DMA_IFCR_CTCIF1;
		DMA1_Channel1->CCR &= ~DMA_CCR_EN;
	}
}

