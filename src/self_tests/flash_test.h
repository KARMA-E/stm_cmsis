/*
 * flash_test.h
 *
 *  Created on: 27 окт. 2021 г.
 *      Author: KARMA
 */
#include "ginc.h"

#ifndef _FLASH_TEST_H_
#define _FLASH_TEST_H_


#define TOGGLE_TEST_GPIO		GPIOC->ODR ^= GPIO_ODR_ODR14

#define	TIME_TEST(func){\
	uint32_t time = SYS_Get_tick_us();\
	TOGGLE_TEST_GPIO;\
	func;\
	TOGGLE_TEST_GPIO;\
	DBG("\r\n%s  process time - %u us", #func,  SYS_Get_tick_us() - time);\
}


#define TEST_AUTO_START		(0)


uint8_t FL_TEST_Check_page(uint16_t page, uint32_t seed);
void 	FL_TEST_Pull(void);
void 	FL_TEST_Set_enable(uint8_t enable);

void 	FL_TEST_Save_statistic(void);
void 	FL_TEST_Read_statistic(void);
void 	FL_TEST_Delete_statistic(void);
void 	FL_TEST_Print_statistic(void);


#endif
